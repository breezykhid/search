#include <iostream>
#include <string>

using namespace std;

int search(string text, string pattern)
{
	int match=0;
	int val=0;
	
	for (int i = 0; i < text.size(); i++)
	{
		if (text[i] == pattern[0])
		{
			val = i + 1;
			match++;
			
			for(int a = 1; a < pattern.size(); a++)
			{
				
				if (text[val] == pattern[a])
				{	
					match++;
					val++;
				}
				else
				{
					match = 0;
					
				}
			}
			if (match == pattern.size())
			{
				return i;
			}
		}
		
	}
	return -1;
}
